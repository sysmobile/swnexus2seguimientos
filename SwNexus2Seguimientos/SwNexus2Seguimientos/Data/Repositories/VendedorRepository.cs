using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SysWork.Data.Common;
using SysWork.Data.GenericRepository;
using SysWork.Data.Common.Attributes;
using SysWork.Data.Common.ValueObjects;
using SwNexus2Seguimientos.Data.Entities;
namespace SwNexus2Seguimientos.Data.Repositories
{
	/// <summary>
	/// This class was created automatically with the RepositoryClassFromDb class.
	/// Inherited from GenericRepository which allows you to perform the following actions: 
	/// Add, 
	/// AddRange, 
	/// Update, 
	/// UpdateRange, 
	/// DeleteById, 
	/// DeleteAll, 
	/// DeleteByLambdaExpressionFilter, 
	/// DeleteByGenericWhereFilter, 
	/// GetById, 
	/// GetByLambdaExpressionFilter, 
	/// GetByGenericWhereFilter, 
	/// GetAll, 
	/// GetListByLambdaExpressionFilter, 
	/// GetDataTableByLambdaExpressionFilter, 
	/// GetListByGenericWhereFilter, 
	/// GetDataTableByGenericWhereFilter, 
	/// Find 
	/// Find 
	/// </summary>

	public class VendedorRepository : BaseRepository<Vendedor>
	{
		public VendedorRepository (string connectionString, EDataBaseEngine dataBaseEngine) : base(connectionString,dataBaseEngine)
		{
		
		}

        public Vendedor GetByDescripcionVendedor(string descripcionVendedor)
        {
            return GetByLambdaExpressionFilter(v => v.descripcionVendedor == descripcionVendedor);
        }
	}
}
