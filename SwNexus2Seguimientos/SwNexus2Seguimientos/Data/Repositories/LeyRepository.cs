using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SysWork.Data.Common;
using SysWork.Data.GenericRepository;
using SysWork.Data.Common.Attributes;
using SysWork.Data.Common.ValueObjects;
using SwNexus2Seguimientos.Data.Entities;
namespace SwNexus2Seguimientos.Data.Repositories
{
	/// <summary>
	/// This class was created automatically with the RepositoryClassFromDb class.
	/// Inherited from GenericRepository which allows you to perform the following actions: 
	/// Add, 
	/// AddRange, 
	/// Update, 
	/// UpdateRange, 
	/// DeleteById, 
	/// DeleteAll, 
	/// DeleteByLambdaExpressionFilter, 
	/// DeleteByGenericWhereFilter, 
	/// GetById, 
	/// GetByLambdaExpressionFilter, 
	/// GetByGenericWhereFilter, 
	/// GetAll, 
	/// GetListByLambdaExpressionFilter, 
	/// GetDataTableByLambdaExpressionFilter, 
	/// GetListByGenericWhereFilter, 
	/// GetDataTableByGenericWhereFilter, 
	/// Find 
	/// Find 
	/// </summary>

	public class LeyRepository : BaseRepository<Ley>
	{
		public LeyRepository (string connectionString, EDataBaseEngine dataBaseEngine) : base(connectionString,dataBaseEngine)
		{
		 
		}

        public Ley GetByDescripcion(string descripcionLey)
        {
            return GetByLambdaExpressionFilter(l=> l.descripcionLey == descripcionLey);
        }


	}
}
