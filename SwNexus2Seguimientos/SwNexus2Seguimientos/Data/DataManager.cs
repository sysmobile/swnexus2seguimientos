﻿using SwNexus2Seguimientos.Data.Repositories;
using System;
using SysWork.Data.GenericDataManager;
using SysWork.Data.GenericDataManager.Intefaces;

namespace SwNexus2Seguimientos.Data
{
    public class DataManager : BaseDataManager<DataManager>, IDataManager
    {
        public AdjuntoRepository  AdjuntoRepository {get; private set; }
        public AdjuntosdniRepository AdjuntosdniRepository { get; private set; }
        public ApoderadoRepository ApoderadoRepository { get; private set; }
        public BeneficiarioRepository BeneficiarioRepository { get; private set; }
        public CodigospostalRepository CodigospostalRepository { get; private set; }
        public EmpresaRepository EmpresaRepository { get; private set; }
        public EstadocivilRepository EstadocivilRepository { get; private set; }
        public EtapaRepository EtapaRepository { get; private set; }
        public ExpedienteRepository ExpedienteRepository { get; private set; }
        public GeneroRepository GeneroRepository { get; private set; }
        public LeyRepository LeyRepository { get; private set; }
        public LlamadaRepository LlamadaRepository { get; private set; }
        public LocalidadRepository LocalidadRepository { get; private set; }
        public LugarRepository LugarRepository { get; private set; }
        public MovimientoRepository MovimientoRepository { get; private set; }
        public PersonaRepository PersonaRepository { get; private set; }
        public ProvinciaRepository ProvinciaRepository { get; private set; }
        public TelefonocategoriaRepository TelefonocategoriaRepository { get; private set; }
        public TelefonoRepository TelefonoRepository { get; private set; }
        public TipotramiteRepository TipotramiteRepository { get; private set; }
        public VendedorRepository VendedorRepository { get; private set; }

        private DataManager()
        {

        }

        public void InitDataObjects()
        {
            AdjuntoRepository = new AdjuntoRepository(ConnectionString,DataBaseEngine);
            AdjuntosdniRepository = new AdjuntosdniRepository(ConnectionString, DataBaseEngine);
            ApoderadoRepository = new ApoderadoRepository(ConnectionString, DataBaseEngine);
            BeneficiarioRepository = new BeneficiarioRepository(ConnectionString, DataBaseEngine);
            CodigospostalRepository = new CodigospostalRepository(ConnectionString, DataBaseEngine);
            EmpresaRepository = new EmpresaRepository(ConnectionString, DataBaseEngine);
            EstadocivilRepository = new EstadocivilRepository(ConnectionString, DataBaseEngine);
            EtapaRepository = new EtapaRepository(ConnectionString, DataBaseEngine);
            ExpedienteRepository = new ExpedienteRepository(ConnectionString, DataBaseEngine);
            GeneroRepository = new GeneroRepository(ConnectionString, DataBaseEngine);
            LeyRepository = new LeyRepository(ConnectionString, DataBaseEngine);
            LlamadaRepository = new LlamadaRepository(ConnectionString, DataBaseEngine);
            LocalidadRepository = new LocalidadRepository(ConnectionString, DataBaseEngine);
            LugarRepository = new LugarRepository(ConnectionString, DataBaseEngine);
            MovimientoRepository = new MovimientoRepository(ConnectionString, DataBaseEngine);
            PersonaRepository = new PersonaRepository(ConnectionString, DataBaseEngine);
            ProvinciaRepository = new ProvinciaRepository(ConnectionString, DataBaseEngine);
            TelefonocategoriaRepository = new TelefonocategoriaRepository(ConnectionString, DataBaseEngine);
            TelefonoRepository = new TelefonoRepository(ConnectionString, DataBaseEngine);
            TipotramiteRepository = new TipotramiteRepository(ConnectionString, DataBaseEngine);
            VendedorRepository = new VendedorRepository(ConnectionString, DataBaseEngine);
        }
    }
}
