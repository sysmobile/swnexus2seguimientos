using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SysWork.Data.Common.Attributes;
namespace SwNexus2Seguimientos.Data.Entities
{
	[DbTable (Name = "expedientes")]
	public class Expediente
	{
	    /// <summary>
	    /// This class was created automatically with the class EntityClassFromDb.
	    /// Please check the DbTypes and the field names.
	    /// </summary>
		[DbColumn(IsIdentity = true, IsPrimary = true)]
		public long idExpediente { get; set; }
		[DbColumn()]
		public long idPersona { get; set; }
		[DbColumn()]
		public long? idTipotramite { get; set; }
		[DbColumn()]
		public string carpeta { get; set; }
		[DbColumn()]
		public string nroExpediente { get; set; }
		[DbColumn()]
		public string anoExpediente { get; set; }
		[DbColumn()]
		public string descripcionExpediente { get; set; }
		[DbColumn()]
		public long? idEtapa { get; set; }
		[DbColumn()]
		public long? idLugar { get; set; }
		[DbColumn()]
		public long? idApoderado { get; set; }
		[DbColumn()]
		public bool eliminado { get; set; }
	}
}
