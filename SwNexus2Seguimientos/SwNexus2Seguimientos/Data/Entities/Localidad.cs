using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SysWork.Data.Common.Attributes;
namespace SwNexus2Seguimientos.Data.Entities
{
	[DbTable (Name = "localidades")]
	public class Localidad
	{
	/// <summary>
	 /// This class was created automatically with the class EntityClassFromDb.
	 /// Please check the DbTypes and the field names.
	 /// </summary>

		[DbColumn(IsIdentity = true, IsPrimary = true)]
		public long idLocalidad { get; set; }
		[DbColumn()]
		public long idProvincia { get; set; }
		[DbColumn()]
		public string descripcionLocalidad { get; set; }
	}
}
