using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SysWork.Data.Common.Attributes;
namespace SwNexus2Seguimientos.Data.Entities
{
	[DbTable (Name = "llamadas")]
	public class Llamada
	{
	    /// <summary>
	    /// This class was created automatically with the class EntityClassFromDb.
	    /// Please check the DbTypes and the field names.
	    /// </summary>

		[DbColumn(IsIdentity = true, IsPrimary = true)]
		public long idLlamada { get; set; }
		[DbColumn()]
		public long idPersona { get; set; }
		[DbColumn()]
		public DateTime fechaLlamada { get; set; }
		[DbColumn()]
		public string descripcionLlamada { get; set; }
		[DbColumn()]
		public DateTime fechaCarga { get; set; }
	}
}
