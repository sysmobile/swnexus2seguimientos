using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SysWork.Data.Common.Attributes;
namespace SwNexus2Seguimientos.Data.Entities
{
	[DbTable (Name = "telefonos")]
	public class Telefono
	{
	/// <summary>
	 /// This class was created automatically with the class EntityClassFromDb.
	 /// Please check the DbTypes and the field names.
	 /// </summary>

		[DbColumn(IsIdentity = true, IsPrimary = true)]
		public long idTelefono { get; set; }
		[DbColumn()]
		public long idPersona { get; set; }
		[DbColumn()]
		public string tipoTelefono { get; set; }
		[DbColumn()]
		public string nroTelefono { get; set; }
		[DbColumn()]
		public string descripcionTelefono { get; set; }
	}
}
