using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SysWork.Data.Common.Attributes;
namespace SwNexus2Seguimientos.Data.Entities
{
	[DbTable (Name = "apoderados")]
	public class Apoderado
	{
	/// <summary>
	 /// This class was created automatically with the class EntityClassFromDb.
	 /// Please check the DbTypes and the field names.
	 /// </summary>

		[DbColumn(IsIdentity = true, IsPrimary = true)]
		public long idApoderado { get; set; }
		[DbColumn()]
		public string descripcionApoderado { get; set; }
		[DbColumn()]
		public string cuit { get; set; }
		[DbColumn()]
		public string matricula { get; set; }
	}
}
