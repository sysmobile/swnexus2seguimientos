using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SysWork.Data.Common.Attributes;
namespace SwNexus2Seguimientos.Data.Entities
{
	[DbTable (Name = "beneficiarios")]
	public class Beneficiario
	{
	/// <summary>
	 /// This class was created automatically with the class EntityClassFromDb.
	 /// Please check the DbTypes and the field names.
	 /// </summary>

		[DbColumn(IsIdentity = true, IsPrimary = true)]
		public long idRelacionbeneficiario { get; set; }
		[DbColumn()]
		public long idCausante { get; set; }
		[DbColumn()]
		public long idBeneficiario { get; set; }
	}
}
