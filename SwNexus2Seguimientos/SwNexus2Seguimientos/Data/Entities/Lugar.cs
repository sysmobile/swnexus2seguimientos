using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SysWork.Data.Common.Attributes;
namespace SwNexus2Seguimientos.Data.Entities
{
	[DbTable (Name = "lugares")]
	public class Lugar
	{
	/// <summary>
	 /// This class was created automatically with the class EntityClassFromDb.
	 /// Please check the DbTypes and the field names.
	 /// </summary>

		[DbColumn(IsIdentity = true, IsPrimary = true)]
		public long idLugar { get; set; }
		[DbColumn()]
		public string descripcionLugar { get; set; }
		[DbColumn()]
		public string longdescripcionLugar { get; set; }
	}
}
