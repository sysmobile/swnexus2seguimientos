using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SysWork.Data.Common.Attributes;
namespace SwNexus2Seguimientos.Data.Entities
{
	[DbTable (Name = "codigospostales")]
	public class Codigospostal
	{
	/// <summary>
	 /// This class was created automatically with the class EntityClassFromDb.
	 /// Please check the DbTypes and the field names.
	 /// </summary>

		[DbColumn(IsIdentity = true, IsPrimary = true)]
		public long idCodpostal { get; set; }
		[DbColumn()]
		public long idLocalidad { get; set; }
		[DbColumn()]
		public string descripcionCodpostal { get; set; }
	}
}
