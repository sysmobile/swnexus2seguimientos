using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SysWork.Data.Common.Attributes;
namespace SwNexus2Seguimientos.Data.Entities
{
	[DbTable (Name = "movimientos")]
	public class Movimiento
	{
	/// <summary>
	 /// This class was created automatically with the class EntityClassFromDb.
	 /// Please check the DbTypes and the field names.
	 /// </summary>

		[DbColumn(IsIdentity = true, IsPrimary = true)]
		public long idMovimiento { get; set; }
		[DbColumn()]
		public long idExpediente { get; set; }
		[DbColumn()]
		public DateTime fechaMovimiento { get; set; }
		[DbColumn()]
		public string descripcionMovimiento { get; set; }
		[DbColumn()]
		public long? idEtapa { get; set; }
		[DbColumn()]
		public long? idLugar { get; set; }
		[DbColumn()]
		public DateTime? vencimiento { get; set; }
		[DbColumn()]
		public DateTime? fechaCarga { get; set; }
	}
}
