using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SysWork.Data.Common.Attributes;
namespace SwNexus2Seguimientos.Data.Entities
{
	[DbTable (Name = "empresas")]
	public class Empresa
	{
	/// <summary>
	 /// This class was created automatically with the class EntityClassFromDb.
	 /// Please check the DbTypes and the field names.
	 /// </summary>

		[DbColumn(IsIdentity = true, IsPrimary = true)]
		public long idEmpresa { get; set; }
		[DbColumn()]
		public string descripcionEmpresa { get; set; }
		[DbColumn()]
		public string cuit { get; set; }
		[DbColumn()]
		public string nombreCalle { get; set; }
		[DbColumn()]
		public string numCalle { get; set; }
		[DbColumn()]
		public string piso { get; set; }
		[DbColumn()]
		public string dpto { get; set; }
		[DbColumn()]
		public long? idProvincia { get; set; }
		[DbColumn()]
		public long? idLocalidad { get; set; }
		[DbColumn()]
		public long? idCodpostal { get; set; }
		[DbColumn()]
		public string telefono { get; set; }
		[DbColumn()]
		public string email { get; set; }
		[DbColumn()]
		public string contacto { get; set; }
		[DbColumn()]
		public long? idVendedor { get; set; }
	}
}
