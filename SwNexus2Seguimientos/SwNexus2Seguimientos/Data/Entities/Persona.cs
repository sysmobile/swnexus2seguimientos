using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SysWork.Data.Common.Attributes;
namespace SwNexus2Seguimientos.Data.Entities
{
	[DbTable (Name = "personas")]
	public class Persona
	{
	/// <summary>
	 /// This class was created automatically with the class EntityClassFromDb.
	 /// Please check the DbTypes and the field names.
	 /// </summary>

		[DbColumn(IsIdentity = true, IsPrimary = true)]
		public long idPersona { get; set; }
		[DbColumn()]
		public string dni { get; set; }
		[DbColumn()]
		public string cuil { get; set; }
		[DbColumn()]
		public string apellido { get; set; }
		[DbColumn()]
		public string nombre { get; set; }
		[DbColumn()]
		public long idGenero { get; set; }
		[DbColumn()]
		public long? idEstadocivil { get; set; }
		[DbColumn()]
		public DateTime? fechaNacimiento { get; set; }
		[DbColumn()]
		public string regimen { get; set; }
		[DbColumn()]
		public long? idLey { get; set; }
		[DbColumn()]
		public string numBeneficio { get; set; }
		[DbColumn()]
		public DateTime? fechaAsignacion { get; set; }
		[DbColumn()]
		public string nombreCalle { get; set; }
		[DbColumn()]
		public string numCalle { get; set; }
		[DbColumn()]
		public string piso { get; set; }
		[DbColumn()]
		public string dpto { get; set; }
		[DbColumn()]
		public long? idProvincia { get; set; }
		[DbColumn()]
		public long? idLocalidad { get; set; }
		[DbColumn()]
		public long? idCodpostal { get; set; }
		[DbColumn()]
		public string email { get; set; }
		[DbColumn()]
		public string clavess { get; set; }
		[DbColumn()]
		public string autonomos { get; set; }
		[DbColumn()]
		public string claveaf { get; set; }
		[DbColumn()]
		public long? idVendedor { get; set; }
		[DbColumn()]
		public string conyuge { get; set; }
		[DbColumn()]
		public decimal aportesVoluntarios { get; set; }
		[DbColumn()]
		public DateTime? fechaaportesVoluntarios { get; set; }
		[DbColumn()]
		public long? idEmpresa { get; set; }
		[DbColumn()]
		public string observaciones { get; set; }
		[DbColumn()]
		public DateTime? fechaDeceso { get; set; }
        [DbColumn()]
        public bool eliminado { get; set; }
        [DbColumn()]
        public long? idSubempresa { get; set; }
        [DbColumn()]
        public string referido { get; set; }
    }
}
