﻿using Microsoft.Office.Interop.Excel;
using SwNexus2Seguimientos.Data;
using SwNexus2Seguimientos.Data.Entities;
using SwNexus2Seguimientos.Data.Repositories;
using System;
using SysWork.Data.Common.ValueObjects;
using Excel = Microsoft.Office.Interop.Excel;

namespace SwNexus2Seguimientos
{
    static class Program
    {
        /// <summary>
        /// Punto de entrada principal para la aplicación.
        /// </summary>

        static Excel.Application _xlApp;
        static Workbook _xlWorkBook;
        static Worksheet _xlWorkSheet;

        static PersonaRepository _personaRepository;
        static LeyRepository _leyRepository;
        static VendedorRepository _vendedorRepository;
        static EmpresaRepository _empresaRepository;
        static TelefonoRepository _telefonoRepository;
        static BeneficiarioRepository _beneficiarioRepository;
        static ExpedienteRepository _expedienteRepository;
        static TipotramiteRepository _tipoTramiteRepository;
        static MovimientoRepository _movimientoRepository;

        enum EExpedientes
        {
            IDPERSONA  = 1,
            CUIL,
            DNI,
            APELLIDO,
            NOMBRE,
            APELLIDO_BENEFICIARIO,
            NOMBRE_BENEFICIARIO,
            CUIL_BENEFICIARIO,
            DNI_BENEFICIARIO,
            BLANCO_1,
            BLANCO_2,
            IDGENERO,
            IDESTADO,
            FECHA_NACIMIENTO,
            REGIMEN,
            IDLEY,
            DESCRIPCION_LEY,
            NUMBENEFICIO,
            FECHA_ASIGNACION,
            NOMBRE_CALLE,
            NUMERO_CALLE,
            PISO,
            DPTO,
            IDPROVINCIA,
            IDLOCALIDAD,
            IDCODIGOPOSTAL,
            EMAIL,
            CLAVESS,
            AUTONOMOS,
            CLAVEAF,
            IDVENDEDOR,
            DESCRIPCION_VENDEDOR,
            CONYUGE,
            APORTES_VOLUNTARIOS,
            FECHA_APORTES_VOLUNTARIOS,
            IDEMPRESA,
            EMPRESA,
            ID_SUBEMPRESA,
            SUBEMPRESA,
            REFERIDO,
            OBSERVACIONES,
            FECHA_DECESO,
            ELIMINADO,
            BLANCO3,
            TELEFONOS,
            BLANCO4,
            BLANCO5,
            BLANCO6,
            CARPETA,
            NROEXPEDIENTE,
            ANIO_EXPEDIENTE,
            BLANCO7,
            BLANCO8,
            IDTIPO_TRAMITE,
            DESCRIPCION_TIPO_TRAMITE,
            IDMOVIMIENTO,
            DESCRIPCION_MOVIMIENTO
        }

        [STAThread]
        static void Main()
        {
            Console.Write("¿Comenzar la importacion?  (S/N)  :");
            var response = Console.ReadLine();
            if (response.ToUpper() == "S")
            {
                ImportData();
            }
            else
            {
                Console.Write("Presione una tecla para salir");
                Console.ReadKey();
            }
        }

        private static void ImportData()
        {
            DataManager.ConnectionString = "server=localhost;database=nexus_v2;user id=root;password=root;persistsecurityinfo=True";
            DataManager.DataBaseEngine = EDataBaseEngine.MySql;

            _personaRepository = DataManager.GetInstance().PersonaRepository;
            _leyRepository = DataManager.GetInstance().LeyRepository;
            _vendedorRepository = DataManager.GetInstance().VendedorRepository;
            _empresaRepository = DataManager.GetInstance().EmpresaRepository;
            _telefonoRepository = DataManager.GetInstance().TelefonoRepository;
            _beneficiarioRepository = DataManager.GetInstance().BeneficiarioRepository;
            _expedienteRepository = DataManager.GetInstance().ExpedienteRepository;
            _tipoTramiteRepository = DataManager.GetInstance().TipotramiteRepository;
            _movimientoRepository = DataManager.GetInstance().MovimientoRepository;

            var archivoExpedientesJudiciales = @"D:\RECIBIR\CLIENTES\NEXUS\PLANILLA-EXPEDIENTES-JUDICIALES-exportacion.xlsx";

            try
            {
                DataManager.GetInstance();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            Console.WriteLine("Conexion Correcta");

            ImportExpedientesJudiciales(archivoExpedientesJudiciales);

            Console.ReadLine();

        }
        private static void ImportExpedientesJudiciales(string archivoExpedientesJudiciales)
        {

            Console.WriteLine("Creando Objetos Excel");
            int filaExcel = 2;

            _xlApp = new Excel.Application();
            _xlWorkBook = _xlApp.Workbooks.Open(archivoExpedientesJudiciales, Type.Missing, false, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing);
            _xlWorkSheet = (Microsoft.Office.Interop.Excel.Worksheet)_xlWorkBook.Worksheets.get_Item(1);

            ExcelRecord excelRecord = GetExcelRecord(_xlWorkSheet, filaExcel);
            while (excelRecord.cuil.Trim() != "")
            {
                Console.Write(".");

                Persona persona = _personaRepository.GetByCuil(excelRecord.cuil);
                bool esAlta = (persona == null);
                if (esAlta)
                {
                    persona = new Persona();

                    persona.dni = excelRecord.dni;
                    persona.cuil = excelRecord.cuil;
                    persona.apellido = excelRecord.apellido;
                    persona.nombre = excelRecord.nombre;
                    persona.idGenero = long.Parse(excelRecord.idGenero);
                    persona.idEstadocivil = null;

                    if (DateTime.TryParse(excelRecord.fechaNacimiento, out DateTime conFechaNacimiento))
                        persona.fechaNacimiento = conFechaNacimiento;

                    persona.regimen = null;

                    Ley ley = _leyRepository.GetByDescripcion(excelRecord.descripcionLey);
                    if (ley == null)
                    {
                        ley = new Ley();
                        ley.descripcionLey = excelRecord.descripcionLey;
                        long idLeyResult = _leyRepository.Add(ley);
                        ley = _leyRepository.GetById(idLeyResult);
                    }
                    persona.idLey = ley.idLey;

                    persona.numBeneficio = string.IsNullOrEmpty(excelRecord.numeroBeneficio) ? null : excelRecord.numeroBeneficio;

                    if (DateTime.TryParse(excelRecord.fechaAsignacion, out DateTime conFechaAsignacion))
                        persona.fechaAsignacion = conFechaAsignacion;

                    persona.nombreCalle = null;
                    persona.numCalle = null;
                    persona.piso = null;
                    persona.dpto = null;
                    persona.idProvincia = null;
                    persona.idLocalidad = null;
                    persona.idCodpostal = null;

                    persona.email = excelRecord.mail;
                    persona.clavess = excelRecord.claveSS;
                    persona.autonomos = excelRecord.autonomos;
                    persona.claveaf = excelRecord.claveAf;

                    if (excelRecord.descripcionVendedor.Trim() != "")
                    {
                        Vendedor vendedor = _vendedorRepository.GetByDescripcionVendedor(excelRecord.descripcionVendedor);
                        if (vendedor == null)
                        {
                            vendedor = new Vendedor();
                            vendedor.descripcionVendedor = excelRecord.descripcionVendedor;
                            long idVendedorResult = _vendedorRepository.Add(vendedor);
                            vendedor = _vendedorRepository.GetById(idVendedorResult);
                        }

                        persona.idVendedor = vendedor.idVendedor;
                    }


                    persona.conyuge = null;

                    if (Decimal.TryParse(excelRecord.aportesVoluntarios, out decimal conAportesVoluntarios))
                        persona.aportesVoluntarios = conAportesVoluntarios;
                    else
                        persona.aportesVoluntarios = Decimal.Parse("0.00");

                    if (DateTime.TryParse(excelRecord.fechaAportesVoluntarios, out DateTime conFechaAportesVoluntarios))
                        persona.fechaaportesVoluntarios = conFechaAportesVoluntarios;

                    Empresa empresa = null;

                    if (excelRecord.descripcionEmpresa.Trim() != "")
                    {
                        empresa = _empresaRepository.GetByDescripcion(excelRecord.descripcionEmpresa);
                        if (empresa == null)
                        {
                            empresa = new Empresa();
                            empresa.descripcionEmpresa = excelRecord.descripcionEmpresa;
                            long idEmpresaResult = _empresaRepository.Add(empresa);
                            empresa = _empresaRepository.GetById(idEmpresaResult);
                        }
                        persona.idEmpresa = empresa.idEmpresa;
                    }

                    persona.observaciones = excelRecord.observaciones;
                    persona.fechaDeceso = null;
                    persona.eliminado = false;

                    if (excelRecord.descripcionSubEmpresa.Trim() != "")
                    {
                        empresa = _empresaRepository.GetByDescripcion(excelRecord.descripcionSubEmpresa);
                        if (empresa == null)
                        {
                            empresa = new Empresa();
                            empresa.descripcionEmpresa = excelRecord.descripcionSubEmpresa;
                            long idEmpresaResult = _empresaRepository.Add(empresa);
                            empresa = _empresaRepository.GetById(idEmpresaResult);
                        }
                        persona.idSubempresa = empresa.idEmpresa;
                    }

                    persona.referido = excelRecord.referido;
                    long idPersonaResult = _personaRepository.Add(persona);

                    // Telefonos
                    string telefonos = excelRecord.telefonos.Trim();
                    if (telefonos != "")
                        ProcesaTelefonos(idPersonaResult, telefonos);

                    // Heredero
                    string cuilBeneficiario = excelRecord.cuilBeneficiario.Trim();
                    if (cuilBeneficiario != excelRecord.cuil.Trim())
                        ProcesaHeredero(idPersonaResult, excelRecord);

                    string descripcionTipoTramite = excelRecord.descripcionTipoTramite;
                    long? idTipoTramite = null;
                    if (descripcionTipoTramite.Trim() != "")
                    {
                        Tipotramite tipoTramite = _tipoTramiteRepository.GetByDescripcion(descripcionTipoTramite);
                        if (tipoTramite == null)
                        {
                            tipoTramite = new Tipotramite();
                            tipoTramite.descripcionTipotramite = descripcionTipoTramite;

                            idTipoTramite = _tipoTramiteRepository.Add(tipoTramite);
                        }
                        else
                        {
                            idTipoTramite = tipoTramite.idTipotramite;
                        }
                    }

                    //Expediente
                    string nroCarpeta = excelRecord.carpeta;
                    Expediente expediente = _expedienteRepository.GetByNroCarpeta(nroCarpeta);
                    long idExpediente = -1;
                    if ((expediente == null) || (nroCarpeta.Trim() == ""))
                    {
                        expediente = new Expediente();

                        expediente.idPersona = idPersonaResult;
                        expediente.idTipotramite = idTipoTramite;
                        expediente.carpeta = excelRecord.carpeta;
                        expediente.nroExpediente = excelRecord.nroExpediente;
                        expediente.anoExpediente = excelRecord.anioExpediente;
                        expediente.descripcionExpediente = "";

                        idExpediente = _expedienteRepository.Add(expediente);
                    }

                    //movimientos
                    if (idExpediente != -1)
                    {
                        Movimiento movimiento = new Movimiento();


                        movimiento.idExpediente = idExpediente;
                        movimiento.fechaMovimiento = DateTime.Today;
                        movimiento.descripcionMovimiento = excelRecord.descripcionMovimiento;
                        movimiento.idEtapa = null;
                        movimiento.idLugar = null;
                        movimiento.vencimiento = null;
                        movimiento.fechaCarga = DateTime.Today;

                        _movimientoRepository.Add(movimiento);

                    }

                }

                filaExcel++;
                excelRecord = GetExcelRecord(_xlWorkSheet, filaExcel);
            }

            Console.WriteLine("Cerrando archivo.");
            _xlWorkBook.Close(null, null, null);
            _xlApp.Quit();

            Console.WriteLine("Liberando memoria.");
            ReleaseObject(_xlWorkSheet);
            ReleaseObject(_xlWorkBook);
            ReleaseObject(_xlApp);

            Console.WriteLine("");
        }

        private static void ProcesaHeredero(long idPersona, ExcelRecord excelRecord)
        {
            Persona heredero = _personaRepository.GetByCuil(excelRecord.cuilBeneficiario);
            if (heredero == null)
            {
                heredero = new Persona();
                heredero.apellido = excelRecord.apellidoBeneficiario;
                heredero.nombre = excelRecord.nombreBeneficiario;
                heredero.cuil = excelRecord.cuilBeneficiario;
                heredero.dni = excelRecord.dniBeneficiario;
                heredero.idGenero = excelRecord.cuilBeneficiario.StartsWith("27") ? 27 : 20;

                long idHerederoResult = _personaRepository.Add(heredero);

                var beneficiario = new Beneficiario();
                beneficiario.idBeneficiario = idHerederoResult;
                beneficiario.idCausante = idPersona;

                _beneficiarioRepository.Add(beneficiario);
            }
        }

        private static void ProcesaTelefonos(long idPersonaResult, string telefonos)
        {
            string[] delimiter = new string[] { "//" };

            int delimiterIndex = telefonos.IndexOf(delimiter[0]);
            if (delimiterIndex == -1)
            {
                delimiter[0] = "/";
                delimiterIndex = telefonos.IndexOf(delimiter[0]);
            }

            if (delimiterIndex != -1)
            {
                string[] nrosTelefonicos = telefonos.Trim().Split(delimiter, StringSplitOptions.RemoveEmptyEntries);
                foreach (var nroTel in nrosTelefonicos)
                {
                    var telefono = new Telefono();
                    telefono.idPersona = idPersonaResult;
                    telefono.tipoTelefono = "OTRO";

                    if (nroTel.Trim().Length > 13)
                    {
                        telefono.nroTelefono = "0000000000000";
                        telefono.descripcionTelefono = nroTel.Trim();
                    }
                    else
                    {
                        telefono.nroTelefono = nroTel;
                        telefono.descripcionTelefono = "";
                    }

                    _telefonoRepository.Add(telefono);
                }
            }
        }

        private static ExcelRecord GetExcelRecord(Worksheet xlWorkSheet, int filaExcel)
        {
            var record = new ExcelRecord();

            record.cuil = safeObjectReader(_xlWorkSheet.Cells[filaExcel, EExpedientes.CUIL].Value);
            record.idPersona = safeObjectReader(_xlWorkSheet.Cells[filaExcel, EExpedientes.IDPERSONA].Value);
            record.dni = safeObjectReader(_xlWorkSheet.Cells[filaExcel, EExpedientes.DNI].Value);
            record.apellido = safeObjectReader(_xlWorkSheet.Cells[filaExcel, EExpedientes.APELLIDO].Value);
            record.nombre = safeObjectReader(_xlWorkSheet.Cells[filaExcel, EExpedientes.NOMBRE].Value);
            record.apellidoBeneficiario = safeObjectReader(_xlWorkSheet.Cells[filaExcel, EExpedientes.APELLIDO_BENEFICIARIO].Value);
            record.nombreBeneficiario = safeObjectReader(_xlWorkSheet.Cells[filaExcel, EExpedientes.NOMBRE_BENEFICIARIO].Value);
            record.cuilBeneficiario = safeObjectReader(_xlWorkSheet.Cells[filaExcel, EExpedientes.CUIL_BENEFICIARIO].Value);
            record.dniBeneficiario = safeObjectReader(_xlWorkSheet.Cells[filaExcel, EExpedientes.DNI_BENEFICIARIO].Value);
            record.blanco1 = safeObjectReader(_xlWorkSheet.Cells[filaExcel, EExpedientes.BLANCO_1].Value);
            record.blanco2 = safeObjectReader(_xlWorkSheet.Cells[filaExcel, EExpedientes.BLANCO_2].Value);
            record.idGenero = safeObjectReader(_xlWorkSheet.Cells[filaExcel, EExpedientes.IDGENERO].Value);
            record.idEstado = safeObjectReader(_xlWorkSheet.Cells[filaExcel, EExpedientes.IDESTADO].Value);
            record.fechaNacimiento = safeObjectReader(_xlWorkSheet.Cells[filaExcel, EExpedientes.FECHA_NACIMIENTO].Value);
            record.regimen = safeObjectReader(_xlWorkSheet.Cells[filaExcel, EExpedientes.REGIMEN].Value);
            record.idLey = safeObjectReader(_xlWorkSheet.Cells[filaExcel, EExpedientes.IDLEY].Value);
            record.descripcionLey = safeObjectReader(_xlWorkSheet.Cells[filaExcel, EExpedientes.DESCRIPCION_LEY].Value);
            record.numeroBeneficio = safeObjectReader(_xlWorkSheet.Cells[filaExcel, EExpedientes.NUMBENEFICIO].Value);
            record.fechaAsignacion = safeObjectReader(_xlWorkSheet.Cells[filaExcel, EExpedientes.FECHA_ASIGNACION].Value);
            record.nombreCalle = safeObjectReader(_xlWorkSheet.Cells[filaExcel, EExpedientes.NOMBRE_CALLE].Value);
            record.numeroCalle = safeObjectReader(_xlWorkSheet.Cells[filaExcel, EExpedientes.NUMERO_CALLE].Value);
            record.piso = safeObjectReader(_xlWorkSheet.Cells[filaExcel, EExpedientes.PISO].Value);
            record.dpto = safeObjectReader(_xlWorkSheet.Cells[filaExcel, EExpedientes.DPTO].Value);
            record.idProcincia = safeObjectReader(_xlWorkSheet.Cells[filaExcel, EExpedientes.IDPROVINCIA].Value);
            record.idLocalidad = safeObjectReader(_xlWorkSheet.Cells[filaExcel, EExpedientes.IDLOCALIDAD].Value);
            record.idCodigoPostal = safeObjectReader(_xlWorkSheet.Cells[filaExcel, EExpedientes.IDCODIGOPOSTAL].Value);
            record.mail = safeObjectReader(_xlWorkSheet.Cells[filaExcel, EExpedientes.EMAIL].Value);
            record.claveSS = safeObjectReader(_xlWorkSheet.Cells[filaExcel, EExpedientes.CLAVESS].Value);
            record.autonomos = safeObjectReader(_xlWorkSheet.Cells[filaExcel, EExpedientes.AUTONOMOS].Value);
            record.claveAf = safeObjectReader(_xlWorkSheet.Cells[filaExcel, EExpedientes.CLAVEAF].Value);
            record.idVendedor = safeObjectReader(_xlWorkSheet.Cells[filaExcel, EExpedientes.IDVENDEDOR].Value);
            record.descripcionVendedor = safeObjectReader(_xlWorkSheet.Cells[filaExcel, EExpedientes.DESCRIPCION_VENDEDOR].Value);
            record.conyuge = safeObjectReader(_xlWorkSheet.Cells[filaExcel, EExpedientes.CONYUGE].Value);
            record.aportesVoluntarios = safeObjectReader(_xlWorkSheet.Cells[filaExcel, EExpedientes.APORTES_VOLUNTARIOS].Value);
            record.fechaAportesVoluntarios = safeObjectReader(_xlWorkSheet.Cells[filaExcel, EExpedientes.FECHA_APORTES_VOLUNTARIOS].Value);
            record.idEmpresa = safeObjectReader(_xlWorkSheet.Cells[filaExcel, EExpedientes.IDEMPRESA].Value);
            record.descripcionEmpresa = safeObjectReader(_xlWorkSheet.Cells[filaExcel, EExpedientes.EMPRESA].Value);
            record.idSubEmpresa = safeObjectReader(_xlWorkSheet.Cells[filaExcel, EExpedientes.ID_SUBEMPRESA].Value);
            record.descripcionSubEmpresa = safeObjectReader(_xlWorkSheet.Cells[filaExcel, EExpedientes.SUBEMPRESA].Value);
            record.referido = safeObjectReader(_xlWorkSheet.Cells[filaExcel, EExpedientes.REFERIDO].Value);
            record.observaciones = safeObjectReader(_xlWorkSheet.Cells[filaExcel, EExpedientes.OBSERVACIONES].Value);
            record.fechaDeceso = safeObjectReader(_xlWorkSheet.Cells[filaExcel, EExpedientes.FECHA_DECESO].Value);
            record.eliminado = safeObjectReader(_xlWorkSheet.Cells[filaExcel, EExpedientes.ELIMINADO].Value);
            record.blanco3 = safeObjectReader(_xlWorkSheet.Cells[filaExcel, EExpedientes.BLANCO3].Value);
            record.telefonos = safeObjectReader(_xlWorkSheet.Cells[filaExcel, EExpedientes.TELEFONOS].Value);
            record.blanco4 = safeObjectReader(_xlWorkSheet.Cells[filaExcel, EExpedientes.BLANCO4].Value);
            record.blanco5 = safeObjectReader(_xlWorkSheet.Cells[filaExcel, EExpedientes.BLANCO5].Value);
            record.blanco6 = safeObjectReader(_xlWorkSheet.Cells[filaExcel, EExpedientes.BLANCO6].Value);
            record.carpeta = safeObjectReader(_xlWorkSheet.Cells[filaExcel, EExpedientes.CARPETA].Value);
            record.nroExpediente = safeObjectReader(_xlWorkSheet.Cells[filaExcel, EExpedientes.NROEXPEDIENTE].Value);
            record.anioExpediente = safeObjectReader(_xlWorkSheet.Cells[filaExcel, EExpedientes.ANIO_EXPEDIENTE].Value);
            record.blanco7 = safeObjectReader(_xlWorkSheet.Cells[filaExcel, EExpedientes.BLANCO7].Value);
            record.blanco8 = safeObjectReader(_xlWorkSheet.Cells[filaExcel, EExpedientes.BLANCO8].Value);
            record.idTipoTramite = safeObjectReader(_xlWorkSheet.Cells[filaExcel, EExpedientes.IDTIPO_TRAMITE].Value);
            record.descripcionTipoTramite = safeObjectReader(_xlWorkSheet.Cells[filaExcel, EExpedientes.DESCRIPCION_TIPO_TRAMITE].Value);
            record.idMovimiento = safeObjectReader(_xlWorkSheet.Cells[filaExcel, EExpedientes.IDMOVIMIENTO].Value);
            record.descripcionMovimiento = safeObjectReader(_xlWorkSheet.Cells[filaExcel, EExpedientes.DESCRIPCION_MOVIMIENTO].Value);

            return record;
        }

        private static void ReleaseObject(object obj)
        {
            try
            {
                System.Runtime.InteropServices.Marshal.ReleaseComObject(obj);
                obj = null;
            }
            catch (Exception ex)
            {
                obj = null;
                Console.WriteLine("Unable to release the Object " + ex.ToString());
            }
            finally
            {
                GC.Collect();
            }
        }

        static string safeObjectReader(object value)
        {
            string result = "";
            if (value != null)
                result = value.ToString();

            return result;
        }

        public class ExcelRecord
        {
            public string cuil { get; set; } 
            public string idPersona { get; set; }
            public string dni { get; set; }
            public string apellido { get; set; }
            public string nombre { get; set; }
            public string apellidoBeneficiario { get; set; }
            public string nombreBeneficiario { get; set; }
            public string cuilBeneficiario { get; set; }
            public string dniBeneficiario { get; set; }
            public string blanco1 { get; set; }
            public string blanco2 { get; set; }
            public string idGenero { get; set; }
            public string idEstado { get; set; }
            public string fechaNacimiento { get; set; }
            public string regimen { get; set; }
            public string idLey { get; set; }
            public string descripcionLey { get; set; }
            public string numeroBeneficio { get; set; }
            public string fechaAsignacion { get; set; }
            public string nombreCalle { get; set; }
            public string numeroCalle { get; set; }
            public string piso { get; set; }
            public string dpto { get; set; }
            public string idProcincia { get; set; }
            public string idLocalidad { get; set; }
            public string idCodigoPostal { get; set; }
            public string mail { get; set; }
            public string claveSS { get; set; }
            public string autonomos { get; set; }
            public string claveAf { get; set; }
            public string idVendedor { get; set; }
            public string descripcionVendedor { get; set; }
            public string conyuge { get; set; }
            public string aportesVoluntarios { get; set; }
            public string fechaAportesVoluntarios { get; set; }
            public string idEmpresa { get; set; }
            public string descripcionEmpresa { get; set; }
            public string idSubEmpresa { get; set; }
            public string descripcionSubEmpresa { get; set; }
            public string referido { get; set; }
            public string observaciones { get; set; }
            public string fechaDeceso { get; set; }
            public string eliminado { get; set; }
            public string blanco3 { get; set; }
            public string telefonos { get; set; }
            public string blanco4 { get; set; }
            public string blanco5 { get; set; }
            public string blanco6 { get; set; }
            public string carpeta { get; set; }
            public string nroExpediente { get; set; }
            public string anioExpediente { get; set; }
            public string blanco7 { get; set; }
            public string blanco8 { get; set; }
            public string idTipoTramite { get; set; }
            public string descripcionTipoTramite { get; set; }
            public string idMovimiento { get; set; }
            public string descripcionMovimiento { get; set; }
        }
    }
}
